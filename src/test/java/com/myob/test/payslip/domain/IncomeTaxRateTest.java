package com.myob.test.payslip.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class IncomeTaxRateTest {

    private static final int RANGE_START = 10_000;
    private static final int RANGE_END = 20_000;

    @Test
    public void shouldStoreValidValues() {
        int initialTax = 1_000;
        BigDecimal rate = new BigDecimal(0.2);
        IncomeTaxRate incomeTaxRate = new IncomeTaxRate(RANGE_START,
                                                        RANGE_END,
                                                        initialTax,
                                                        rate);
        assertThat(incomeTaxRate.getRangeStart(), equalTo(RANGE_START));
        assertThat(incomeTaxRate.getRangeEnd(), equalTo(RANGE_END));
        assertThat(incomeTaxRate.getInitialTax(), equalTo(initialTax));
        assertThat(incomeTaxRate.getRate(), equalTo(rate));
    }

    @Test
    public void shouldAcceptZeroRangeStartAndInitialTaxAndRate() {
        IncomeTaxRate incomeTaxRate = new IncomeTaxRate(0,
                                                        RANGE_END,
                                                        0,
                                                        ZERO);
        assertThat(incomeTaxRate.getRangeStart(), equalTo(0));
        assertThat(incomeTaxRate.getInitialTax(), equalTo(0));
        assertThat(incomeTaxRate.getRate(), equalTo(ZERO));
    }

    @Test
    public void shouldIncludeValueWithinRange() {
        assertThat(createIncomeTaxRate().includes(RANGE_START + 1), equalTo(true));
    }

    @Test
    public void shouldNotIncludeValueBelowRange() {
        assertThat(createIncomeTaxRate().includes(RANGE_START - 1), equalTo(false));
    }

    @Test
    public void shouldNotIncludeValueAboveRange() {
        assertThat(createIncomeTaxRate().includes(RANGE_END + 1), equalTo(false));
    }

    @Test
    public void shouldIncludeRangeStart() {
        assertThat(createIncomeTaxRate().includes(RANGE_START), equalTo(true));
    }

    @Test
    public void shouldIncludeRangeEnd() {
        assertThat(createIncomeTaxRate().includes(RANGE_END), equalTo(true));
    }

    @Test
    public void rateCutoverShouldBeBasedOnRangeStart() {
        assertThat(createIncomeTaxRate().getRateCutover(), equalTo(RANGE_START - 1));
    }


    @Test
    public void rateCutoverForZeroRangeStartShouldBeZero() {
        IncomeTaxRate incomeTaxRate = new IncomeTaxRate(0,
                                                        RANGE_END,
                                                        1_000,
                                                        new BigDecimal("0.2"));
        assertThat(incomeTaxRate.getRateCutover(), equalTo(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNegativeRangeStart() {
        new IncomeTaxRate(-1,
                          RANGE_END,
                          1_000,
                          new BigDecimal("0.2"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptRangeEndLowerThanRangeStart() {
        new IncomeTaxRate(RANGE_START,
                          RANGE_START - 1,
                          1_000,
                          new BigDecimal("0.2"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptRangeEndEqualToRangeStart() {
        new IncomeTaxRate(RANGE_START,
                          RANGE_START,
                          1_000,
                          new BigDecimal("0.2"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNegativeInitialTax() {
        new IncomeTaxRate(RANGE_START,
                          RANGE_END,
                          -1,
                          new BigDecimal("0.2"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNegativeRate() {
        new IncomeTaxRate(RANGE_START,
                          RANGE_END,
                          1_000,
                          new BigDecimal("-0.1"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptRateGreaterThanOne() {
        new IncomeTaxRate(RANGE_START,
                          RANGE_END,
                          1_000,
                          new BigDecimal("1.1"));
    }

    private IncomeTaxRate createIncomeTaxRate() {
        return new IncomeTaxRate(RANGE_START,
                                 RANGE_END,
                                 1_000,
                                 new BigDecimal("0.2"));
    }
}