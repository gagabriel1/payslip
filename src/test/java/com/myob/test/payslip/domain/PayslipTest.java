package com.myob.test.payslip.domain;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class PayslipTest {

    @Test
    public void grossSalaryShouldBeRoundedDown() throws Exception {
        Employee employee = employeeFor(12_005);
        Payslip payslip = new Payslip(employee, "March", fullTaxRates());
        assertThat(payslip.getGrossIncome(), equalTo(1_000));
    }

    @Test
    public void grossSalaryShouldBeRoundedUp() throws Exception {
        Employee employee = employeeFor(12_006);
        Payslip payslip = new Payslip(employee, "March", fullTaxRates());
        assertThat(payslip.getGrossIncome(), equalTo(1_001));
    }

    @Test
    public void incomeTaxShouldBeCorrectRoundingDown() throws Exception {
        int annualIncome = 22_000;
        int rateCutover = 10_000;
        int initialTax = 125;
        BigDecimal rate = new BigDecimal("0.1");
        Employee employee = employeeFor(annualIncome);
        Payslip payslip = new Payslip(employee, "March",
                                      taxRatesFor(annualIncome, rateCutover, initialTax, rate));
        // incomeTax = (125 + (22_000 - 10_000)*0.1) / 12 = 110.4... = 110
        assertThat(payslip.getIncomeTax(), equalTo(110));
    }

    @Test
    public void incomeTaxShouldBeCorrectRoundingUp() throws Exception {
        int annualIncome = 22_000;
        int rateCutover = 10_000;
        int initialTax = 126;
        BigDecimal rate = new BigDecimal("0.1");
        Employee employee = employeeFor(annualIncome);
        Payslip payslip = new Payslip(employee, "March",
                                      taxRatesFor(annualIncome, rateCutover, initialTax, rate));
        // incomeTax = (126 + (22_000 - 10_000)*0.1) / 12 = 110.5 = 111
        assertThat(payslip.getIncomeTax(), equalTo(111));
    }

    @Test
    public void netIncomeShouldBeGrossIncomeLessTax() throws Exception {
        Employee employee = employeeFor(120_000);
        Payslip payslip = new Payslip(employee, "March", fullTaxRates());
        final int grossIncome = payslip.getGrossIncome();
        final int incomeTax = payslip.getIncomeTax();
        assertThat(grossIncome, greaterThan(0));
        assertThat(incomeTax, greaterThan(0));
        assertThat(payslip.getNetIncome(), equalTo(grossIncome - incomeTax));
    }

    @Test
    public void superShouldBeRoundedDown() throws Exception {
        Employee employee = employeeFor(12_050, new BigDecimal("0.1"));
        Payslip payslip = new Payslip(employee, "March", fullTaxRates());
        assertThat(payslip.getGrossIncome(), equalTo(1004));
        assertThat(payslip.getSuperannuation(), equalTo(100));
    }

    @Test
    public void superShouldBeRoundedUp() throws Exception {
        Employee employee = employeeFor(12_060, new BigDecimal("0.1"));
        Payslip payslip = new Payslip(employee, "March", fullTaxRates());
        assertThat(payslip.getSuperannuation(), equalTo(101));
    }

    private Employee employeeFor(int annualSalary) {
        return employeeFor(annualSalary, BigDecimal.ZERO);
    }

    private Employee employeeFor(int annualSalary, BigDecimal superRate) {
        return new Employee("John", "Doe", annualSalary, superRate);
    }

    private IncomeTaxRates taxRatesFor(final int annualIncome, final int rateCutover, final int initialTax,
                                       final BigDecimal rate) {
        return new IncomeTaxRates(() -> ImmutableList.of(new IncomeTaxRate(rateCutover + 1, annualIncome,
                                                                           initialTax, rate)));
    }

    private IncomeTaxRates fullTaxRates() {
        return new IncomeTaxRates(new StaticIncomeTaxRatesProvider());
    }

}