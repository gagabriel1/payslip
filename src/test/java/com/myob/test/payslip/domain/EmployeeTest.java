package com.myob.test.payslip.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class EmployeeTest {

    @Test
    public void shouldGetFullName() {
        Employee employee = new Employee("Darth", "Vader", 1, BigDecimal.ZERO);
        assertThat(employee.getFullName(), equalTo("Darth Vader"));
    }
}