package com.myob.test.payslip.domain;

import org.junit.Test;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.MAX_VALUE;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class StaticIncomeTaxRatesProviderTest {

    private final StaticIncomeTaxRatesProvider taxRatesProvider = new StaticIncomeTaxRatesProvider();

    @Test
    public void shouldHaveTaxRateForAllPossibleIncomeValues() throws Exception {
        List<IncomeTaxRate> taxRates = taxRatesProvider.getIncomeTaxRates();
        taxRates = sortByRangeStartAscending(taxRates);
        assertThat(taxRates, hasSize(greaterThan(0)));
        Iterator<IncomeTaxRate> iterator = taxRates.iterator();
        IncomeTaxRate taxRate = iterator.next();
        while(iterator.hasNext()) {
            IncomeTaxRate followingTaxRate = iterator.next();
            assertThat(taxRate.getRangeEnd(), equalTo(followingTaxRate.getRangeStart() - 1));
            taxRate = followingTaxRate;
        }
        assertThat(taxRate.getRangeEnd(), equalTo(MAX_VALUE));
    }

    private List<IncomeTaxRate> sortByRangeStartAscending(List<IncomeTaxRate> taxRates) {
        return taxRates.stream()
                       .sorted(Comparator.comparing(IncomeTaxRate::getRangeStart))
                       .collect(Collectors.toList());
    }
}