package com.myob.test.payslip.domain;

import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class IncomeTaxRatesTest {

    private final IncomeTaxRates taxRates = new IncomeTaxRates(new StaticIncomeTaxRatesProvider());

    @Test
    public void shouldGetRateForAnnualIncome() throws Exception {
        int income = 10_000;
        IncomeTaxRate rate = taxRates.getRateForAnnualIncome(income);
        assertThat(rate.includes(income), equalTo(true));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getRateForAnnualIncomeShouldNotAcceptNegativeValue() throws Exception {
        taxRates.getRateForAnnualIncome(-1);
    }
}