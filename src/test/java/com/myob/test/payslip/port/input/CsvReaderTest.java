package com.myob.test.payslip.port.input;

import com.myob.test.payslip.application.EmployeePayment;
import org.junit.Test;

import java.io.StringReader;
import java.math.BigDecimal;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class CsvReaderTest {

    @Test
    public void shouldParseCorrectInfoFromInput() throws Exception {
        StringReader reader = new StringReader("David,Rudd,60050,9%,01 March – 31 March");
        CsvReader csvReader = new CsvReader(reader);
        assertThat(csvReader.hasNext(), equalTo(true));
        EmployeePayment employeePayment = csvReader.next();
        assertThat(employeePayment.getFirstName(), equalTo("David"));
        assertThat(employeePayment.getLastName(), equalTo("Rudd"));
        assertThat(employeePayment.getAnnualSalary(), equalTo(60050));
        assertThat(employeePayment.getSuperRate(), equalTo(new BigDecimal("0.09")));
        assertThat(employeePayment.getPaymentPeriod(), equalTo("01 March – 31 March"));
    }

    @Test
    public void shouldParseAllRowsFromInput() throws Exception {
        StringReader reader = new StringReader("David,Rudd,60050,9%,01 March – 31 March");
        CsvReader csvReader = new CsvReader(reader);
        assertThat(csvReader.hasNext(), equalTo(true));
        EmployeePayment employeePayment = csvReader.next();
        assertThat(employeePayment.getFirstName(), equalTo("David"));
        assertThat(employeePayment.getLastName(), equalTo("Rudd"));
        assertThat(employeePayment.getAnnualSalary(), equalTo(60050));
        assertThat(employeePayment.getSuperRate(), equalTo(new BigDecimal("0.09")));
        assertThat(employeePayment.getPaymentPeriod(), equalTo("01 March – 31 March"));
    }

    @Test
    public void shouldAcceptSuperRateWithDecimals() throws Exception {
        StringReader reader = new StringReader("David,Rudd,60050,9.12345%,01 March – 31 March");
        CsvReader csvReader = new CsvReader(reader);
        assertThat(csvReader.hasNext(), equalTo(true));
        assertThat(csvReader.next().getSuperRate(), equalTo(new BigDecimal("0.0912345")));
    }

}