package com.myob.test.payslip.bootstrap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class FilePayslipProcessorIT {

    private static final String OUTPUT_FILENAME = "input-test-out.csv";
    private static final String INPUT_FILENAME = "input-test.csv";

    @Before
    @After
    public void removeOutputFile() throws Exception {
        URL resource = this.getClass().getClassLoader().getResource(OUTPUT_FILENAME);
        if (resource != null) {
            Files.delete(Paths.get(resource.toURI()));
        }
    }

    @Test
    public void shouldProduceExpectedOutput() throws Exception {
        final URL input = this.getClass().getClassLoader().getResource(INPUT_FILENAME);
        final Path inputPath = Paths.get(input.toURI());
        new FilePayslipProcessor().run(inputPath);
        List<String> expected = readAllLinesFrom("expected-output.csv");
        List<String> actual = readAllLinesFrom(OUTPUT_FILENAME);
        assertThat(actual, equalTo(expected));
    }

    private List<String> readAllLinesFrom(String filename) throws Exception {
        return Files.readAllLines(Paths.get(this.getClass().getClassLoader().getResource(filename).toURI()));
    }
}