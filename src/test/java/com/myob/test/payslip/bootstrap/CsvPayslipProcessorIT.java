package com.myob.test.payslip.bootstrap;

import org.junit.Test;

import java.io.StringReader;
import java.io.StringWriter;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class CsvPayslipProcessorIT {

    @Test
    public void shouldProduceExpectedOutput() throws Exception {
        StringReader reader = new StringReader(getInput());
        StringWriter writer = new StringWriter();
        new CsvPayslipProcessor().run(reader, writer);
        // the splitting is just to avoid comparing the line separator
        String[] expected = writer.toString().split("[\r\n]+");
        String[] actual = getExpectedOutput().split("[\r\n]+");
        assertThat(actual, equalTo(expected));
    }

    private String getInput() {
        return "David,Rudd,60050,9%,01 March – 31 March\n" +
                "Ryan,Chen,120000,10%,01 March – 31 March";
    }

    public String getExpectedOutput() {
        return "David Rudd,01 March – 31 March,5004,922,4082,450\n" +
                "Ryan Chen,01 March – 31 March,10000,2696,7304,1000";
    }
}