package com.myob.test.payslip.bootstrap;

import java.nio.file.Paths;

/**
 * It takes a input file location as argument to read the input data, using a {@see FilePayslipProcessor}
 * to generate the output.
 */
public class CliPayslipProcessor {

    public static void main(String[] args) {
        checkArguments(args);
        try {
            new FilePayslipProcessor().run(Paths.get(args[0]));
            System.out.println("Successfully generated output!");
        } catch (ProcessingException | IllegalArgumentException e) {
            System.out.println("Error processing file:");
            printMessages(e);
            System.out.println("\nDetailed error:");
            e.printStackTrace(System.out);
            System.exit(1);
        }
    }

    private static void printMessages(Throwable throwable) {
        Throwable parent;
        String prefix = "";
        do {
            prefix = prefix + " ";
            System.out.println(prefix + throwable.getMessage());
            parent = throwable;
            throwable = throwable.getCause();
        } while(throwable != null && throwable != parent);
    }

    private static void checkArguments(String[] args) {
        if (args.length != 1) {
            System.out.println("Error: missing input file.");
            System.exit(1);
        }
    }

}
