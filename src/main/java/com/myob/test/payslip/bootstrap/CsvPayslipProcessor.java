package com.myob.test.payslip.bootstrap;

import com.myob.test.payslip.application.GeneratePayslipsCommand;
import com.myob.test.payslip.application.PayslipGenerationApplication;
import com.myob.test.payslip.domain.IncomeTaxRates;
import com.myob.test.payslip.domain.StaticIncomeTaxRatesProvider;
import com.myob.test.payslip.port.input.CsvReader;
import com.myob.test.payslip.port.input.CsvReaderException;
import com.myob.test.payslip.port.output.CsvPayslipRepository;

import java.io.Reader;
import java.io.Writer;

/**
 * Parses a the CSV content of the input and writes the result as a CSV content to the output.
 * Input should have proper format {@see CsvReader}.
 */
public class CsvPayslipProcessor {

    public void run(Reader in, Writer out) {
        CsvPayslipRepository csvPayslipRepository = new CsvPayslipRepository(out);
        IncomeTaxRates incomeTaxRates = new IncomeTaxRates(new StaticIncomeTaxRatesProvider());
        PayslipGenerationApplication application = new PayslipGenerationApplication(incomeTaxRates,
                                                                                    csvPayslipRepository);
        try {
            CsvReader csvReader = new CsvReader(in);
            application.generatePayslips(new GeneratePayslipsCommand(csvReader));
            csvPayslipRepository.close();
        } catch (CsvReaderException e) {
            throw new ProcessingException("Error reading or parsing the input content", e);
        }
    }
}
