package com.myob.test.payslip.bootstrap;

import com.google.common.io.Files;

import java.io.*;
import java.nio.file.Path;

import static java.lang.String.format;

/**
 * It gets the content of the input file and writes the result to a file in the same location,
 * appending "-out.csv" to the input file name minus the extension.
 * {@see CsvPayslipProcessor} for details of the expected input and the generated output.
 */
public class FilePayslipProcessor {

    private static final String OUTPUT_SUFFIX = "-out.csv";
    private final CsvPayslipProcessor processor;

    public FilePayslipProcessor() {
        this.processor = new CsvPayslipProcessor();
    }

    /**
     * Does the actual processing of the input file and writing to the output file.
     *
     * @param inputFile the input file, must exist and be readable
     * @throws ProcessingException if there is any error opening or reading or writing to the input and output files
     */
    public void run(Path inputFile) {
        Reader reader = getReader(inputFile);
        Path outputFilePath = getOutputFile(inputFile);
        Writer writer = getWriter(outputFilePath);
        this.processor.run(reader, writer);
    }

    private static Path getOutputFile(Path inputFilePath) {
        Path inputParent = inputFilePath.toAbsolutePath().getParent();
        String inputFilename = Files.getNameWithoutExtension(inputFilePath.toString());
        String outputFilename = inputFilename + OUTPUT_SUFFIX;
        return inputParent.resolve(outputFilename);
    }

    private static Reader getReader(Path inputFile) {
        try {
            return new BufferedReader(new FileReader(inputFile.toFile()));
        } catch (FileNotFoundException e) {
            throw new ProcessingException(format("File %s not found", inputFile), e);
        }
    }

    private static Writer getWriter(Path outputFile) {
        try {
            return new BufferedWriter(new FileWriter(outputFile.toFile()));
        } catch (IOException e) {
            throw new ProcessingException(format("Cannot write to file %s", outputFile), e);
        }
    }
}
