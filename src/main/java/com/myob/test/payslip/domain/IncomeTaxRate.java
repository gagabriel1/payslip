package com.myob.test.payslip.domain;

import java.math.BigDecimal;

import static com.google.common.base.Preconditions.checkArgument;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;

/**
 * Income tax details for a given income range.
 * <p>
 * Uses integer numbers for salaries, which prevents from using salaries with cents. But this is
 * how the tax ranges are specified by ATO.
 */
public class IncomeTaxRate {

    private int rangeStart;
    private int rangeEnd;
    private int initialTax;
    private BigDecimal rate;

    /**
     * @param rangeStart lowest annual income to include in this rate, should be 0 or positive
     * @param rangeEnd   highest annual income to include in this rate, should be greater than rangeStart
     * @param initialTax fixed initial tax amount for this rate, should be 0 or positive
     * @param rate       tax rate to apply to the income exceeding the rate cutover; should come as a fraction, i.e,
     *                   9% should come as 0.09, should be between 0 and 1, including any of them
     *                   
     * @throws IllegalArgumentException if any of the inputs is invalid
     */
    public IncomeTaxRate(int rangeStart, int rangeEnd, int initialTax, BigDecimal rate) {
        checkArgument(rangeStart >= 0, "rangeStart must be 0 or positive, was %s", rangeStart);
        this.rangeStart = rangeStart;
        checkArgument(rangeEnd > rangeStart,
                      "rangeEnd must be greater than rangeStart, they were %s and %s", rangeEnd, rangeStart);
        this.rangeEnd = rangeEnd;
        checkArgument(initialTax >= 0, "initialTax must be 0 or positive, was %s", initialTax);
        this.initialTax = initialTax;
        checkArgument(rate.compareTo(ZERO) >= 0 && rate.compareTo(ONE) <= 0,
                      "rate must be between 0 and 1, was %s", rate);
        this.rate = rate;
    }

    /**
     * @param annualIncome the annual income
     * @return true if the annual income is in this range, false otherwise
     */
    public boolean includes(int annualIncome) {
        return rangeStart <= annualIncome && annualIncome <= rangeEnd;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }

    public int getInitialTax() {
        return initialTax;
    }

    public BigDecimal getRate() {
        return rate;
    }

    /**
     * @return the annual income above which the rate should be applied
     */
    public int getRateCutover() {
        return Math.max(0, rangeStart - 1);
    }
}
