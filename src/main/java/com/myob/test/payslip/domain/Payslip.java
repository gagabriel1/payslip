package com.myob.test.payslip.domain;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;

/**
 * This class makes the calculation of the taxes and monthly salary for the given employee.
 * <p>
 * It can only calculate salaries for a whole and single month. So it is not possible to calculate the salary of a
 * new employee or for any other variation of partial month.
 */
public class Payslip {

    private static final BigDecimal MONTHS_IN_YEAR = BigDecimal.valueOf(12);

    private final Employee employee;
    private final String payPeriod;
    private final int grossIncome;
    private final int incomeTax;
    private final int netIncome;
    private final int superannuation;

    /**
     * @param employee
     * @param payPeriod      a string representing the pay period; this is not validated or used anywhere so could
     *                       potentially be anything
     * @param incomeTaxRates the tax rate table to use to calculate the applicable tax
     */
    public Payslip(Employee employee, String payPeriod, IncomeTaxRates incomeTaxRates) {
        // none of the arguments should be null; if any is, it is a bug, so it is OK for the app to crash
        this.employee = employee;
        this.payPeriod = payPeriod;
        this.grossIncome = calculateGrossIncome(employee);
        this.incomeTax = calculateIncomeTax(employee, incomeTaxRates);
        this.netIncome = calculateNetIncome(grossIncome, incomeTax);
        this.superannuation = calculateSuperannuation(employee, grossIncome);
    }

    private int calculateGrossIncome(Employee employee) {
        BigDecimal annualSalary = BigDecimal.valueOf(employee.getAnnualSalary());
        return annualSalary.divide(MONTHS_IN_YEAR, 0, HALF_UP).intValue();
    }

    private int calculateIncomeTax(Employee employee, IncomeTaxRates incomeTaxRates) {
        int annualIncome = employee.getAnnualSalary();
        IncomeTaxRate applicableTaxRate = incomeTaxRates.getRateForAnnualIncome(annualIncome);
        BigDecimal grossIncome = BigDecimal.valueOf(employee.getAnnualSalary());
        BigDecimal initialTax = BigDecimal.valueOf(applicableTaxRate.getInitialTax());
        BigDecimal rate = applicableTaxRate.getRate();
        BigDecimal rateCutover = BigDecimal.valueOf(applicableTaxRate.getRateCutover());

        return initialTax.add(grossIncome.subtract(rateCutover).multiply(rate))
                         .divide(MONTHS_IN_YEAR, 0, HALF_UP).intValue();
    }

    private int calculateNetIncome(int grossIncome, int incomeTax) {
        return grossIncome - incomeTax;
    }

    private int calculateSuperannuation(Employee employee, int grossIncome) {
        BigDecimal superRate = employee.getSuperRate();
        BigDecimal grossIncomeDecimal = BigDecimal.valueOf(grossIncome);
        return grossIncomeDecimal.multiply(superRate).setScale(0, HALF_UP).intValue();
    }

    public Employee getEmployee() {
        return employee;
    }

    public String getPayPeriod() {
        return payPeriod;
    }

    public int getIncomeTax() {
        return incomeTax;
    }

    public int getGrossIncome() {
        return grossIncome;
    }

    public int getNetIncome() {
        return netIncome;
    }

    public int getSuperannuation() {
        return superannuation;
    }
}
