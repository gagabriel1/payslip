package com.myob.test.payslip.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Creates the tax rates using the hard coded values.
 * Based on the values at ATO (https://www.ato.gov.au/rates/individual-income-tax-rates/):
 * <p>
 * 0 - $18,200             Nil
 * $18,201 - $37,000       19c for each $1 over $18,200
 * $37,001 - $80,000       $3,572 plus 32.5c for each $1 over $37,000
 * $80,001 - $180,000      $17,547 plus 37c for each $1 over $80,000
 * $180,001 and over       $54,547 plus 45c for each $1 over $180,000
 */
public class StaticIncomeTaxRatesProvider implements IncomeTaxRatesProvider {

    private final List<IncomeTaxRate> taxRates;

    public StaticIncomeTaxRatesProvider() {
        this.taxRates = Collections.unmodifiableList(createTaxRates());
    }

    private List<IncomeTaxRate> createTaxRates() {
        List<IncomeTaxRate> rates = new ArrayList<>();
        rates.add(new IncomeTaxRate(0,
                                    18_200,
                                    0,
                                    BigDecimal.ZERO));
        rates.add(new IncomeTaxRate(18_201,
                                    37_000,
                                    0,
                                    new BigDecimal("0.19")));
        rates.add(new IncomeTaxRate(37_001,
                                    80_000,
                                    3_572,
                                    new BigDecimal("0.325")));
        rates.add(new IncomeTaxRate(80_001,
                                    180_000,
                                    17_547,
                                    new BigDecimal("0.37")));
        /*
        It could be more elegant to have another IncomeTaxRate implementation that would
        represent only a start value instead of using a MAX_VALUE, but this is simple enough
        that this should be OK.
         */
        rates.add(new IncomeTaxRate(180_001,
                                    Integer.MAX_VALUE,
                                    54_547,
                                    new BigDecimal("0.45")));
        return rates;
    }

    @Override
    public List<IncomeTaxRate> getIncomeTaxRates() {
        return taxRates;
    }
}
