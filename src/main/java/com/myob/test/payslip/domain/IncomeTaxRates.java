package com.myob.test.payslip.domain;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

/**
 * Provides the ability to search for the applicable tax rate to use for a given income.
 */
public class IncomeTaxRates {

    private final List<IncomeTaxRate> taxRates;

    /**
     * @param provider the provider of the tax rates to use; should return a complete list of rates for all possible
     *                 annual incomes
     */
    public IncomeTaxRates(IncomeTaxRatesProvider provider) {
        this.taxRates = Collections.unmodifiableList(provider.getIncomeTaxRates());
    }

    /**
     * @param annualIncome the annual income whose applicable is requested, should be 0 or positive
     * @return the applicable tax rate to use for the given income
     * @throws IllegalArgumentException if the annual income is negative
     * @throws IllegalStateException    if there is no rate applicable to the given income; this represents
     *                                  a corrupted table and most likely a serious application defect
     */
    public IncomeTaxRate getRateForAnnualIncome(final int annualIncome) {
        if (annualIncome < 0) {
            throw new IllegalArgumentException(format("Annual income must be 0 or positive, was: %s", annualIncome));
        }
        Optional<IncomeTaxRate> maybeTaxRate = this.taxRates.stream()
                                                            .filter(r -> r.includes(annualIncome))
                                                            .findFirst();
        return maybeTaxRate.orElseThrow(() -> new IllegalStateException(
                format("Income tax table is corrupted, it does not include a tax rate for %s", annualIncome)));
    }
}
