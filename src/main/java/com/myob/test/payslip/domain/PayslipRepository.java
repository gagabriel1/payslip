package com.myob.test.payslip.domain;

/**
 * Knows how to store a payslip
 */
public interface PayslipRepository {

    /**
     * Stores a payslip
     *
     * @param payslip the payslip to store
     *
     * @throws PayslipRepositoryException if there is any error saving the payslip
     */
    void save(Payslip payslip);
}
