package com.myob.test.payslip.domain;

import java.math.BigDecimal;

import static java.lang.String.format;

public class Employee {
    private final String firstName;
    private final String lastName;
    private final int annualSalary;
    private final BigDecimal superRate;

    public Employee(String firstName, String lastName, int annualSalary, BigDecimal superRate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.annualSalary = annualSalary;
        this.superRate = superRate;
    }

    public int getAnnualSalary() {
        return annualSalary;
    }

    public BigDecimal getSuperRate() {
        return superRate;
    }

    public String getFullName() {
        return format("%s %s", firstName, lastName);
    }
}
