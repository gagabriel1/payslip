package com.myob.test.payslip.domain;

public class PayslipRepositoryException extends RuntimeException {

    public PayslipRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
