package com.myob.test.payslip.domain;

import java.util.List;

/**
 * Retrieves the tax rates to use.
 * <p>
 * Implementations should return a complete list of rates for all possible annual incomes.
 */
public interface IncomeTaxRatesProvider {

    List<IncomeTaxRate> getIncomeTaxRates();
}
