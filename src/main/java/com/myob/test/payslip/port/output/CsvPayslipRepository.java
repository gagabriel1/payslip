package com.myob.test.payslip.port.output;

import com.myob.test.payslip.domain.Payslip;
import com.myob.test.payslip.domain.PayslipRepository;
import com.myob.test.payslip.domain.PayslipRepositoryException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.Writer;

/**
 * Saves payslips to given writer as a CSV file. It does not save headers. Each row will have:
 * <ol>
 * <li>employee full name: as first name plus last name </li>
 * <li>pay period: just as received, without any additional formatting</li>
 * <li>gross monthly income: an integer value without any currency symbol</li>
 * <li>monthly income tax: an integer value without any currency symbol</li>
 * <li>net monthly income: an integer value without any currency symbol</li>
 * <li>monthly superannuation: an integer value without any currency symbol</li>
 * </ol>
 * <p>
 * This should be closed once it is not used anymore.
 */
public class CsvPayslipRepository implements PayslipRepository {

    private final CSVPrinter csvPrinter;

    public CsvPayslipRepository(Writer writer) {
        try {
            csvPrinter = CSVFormat.DEFAULT.print(writer);
        } catch (IOException e) {
            throw new PayslipRepositoryException("Cannot write to writer", e);
        }
    }

    /**
     * Saves the payslip
     *
     * @param payslip the payslip to store
     * @throws PayslipRepositoryException if there is any issue saving the payslip
     */
    @Override
    public void save(Payslip payslip) {
        try {
            csvPrinter.printRecord(payslip.getEmployee().getFullName(),
                                   payslip.getPayPeriod(),
                                   payslip.getGrossIncome(),
                                   payslip.getIncomeTax(),
                                   payslip.getNetIncome(),
                                   payslip.getSuperannuation());
        } catch (IOException e) {
            throw new PayslipRepositoryException("Error writing to output writer", e);
        }
    }

    /**
     * Closes the writer.
     *
     * @throws PayslipRepositoryException if there is any issue while closing
     */
    public void close() {
        try {
            this.csvPrinter.close();
        } catch (IOException e) {
            throw new PayslipRepositoryException("Cannot close output writer", e);
        }
    }
}
