package com.myob.test.payslip.port.input;

public class CsvReaderException extends RuntimeException {

    public CsvReaderException(String message) {
        super(message);
    }

    public CsvReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
