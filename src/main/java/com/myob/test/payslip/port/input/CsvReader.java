package com.myob.test.payslip.port.input;

import com.myob.test.payslip.application.EmployeePayment;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Iterator;

import static java.lang.String.format;

/**
 * Parses CSV content from the given input. It expects the content to not have headers. Each row should have:
 * <ol>
 *     <li>employee first name</li>
 *     <li>employee last name</li>
 *     <li>annual salary: an integer value without any currency symbol</li>
 *     <li>super rate: percentage of super applicable to the employee, should end with a '%' symbol</li>
 *     <li>payment period: a string representing the payment period, can have any format as it is not parsed</li>
 * </ol>
 */
public class CsvReader implements Iterator<EmployeePayment> {

    private static final BigDecimal _100 = BigDecimal.valueOf(100);
    private final Iterator<CSVRecord> csvRecords;

    public CsvReader(Reader reader) {
        Iterable<CSVRecord> records;
        try {
            records = CSVFormat.RFC4180.parse(reader);
        } catch (IOException e) {
            throw new CsvReaderException("Error reading CSV records", e);
        }
        this.csvRecords = records.iterator();
    }

    @Override
    public boolean hasNext() {
        return csvRecords.hasNext();
    }

    @Override
    public EmployeePayment next() {
        return parseRecord(csvRecords.next());
    }

    private EmployeePayment parseRecord(CSVRecord record) {
        try {
            String firstName = record.get(0);
            String lastName = record.get(1);
            int annualSalary = toAnnualSalary(record.get(2));
            BigDecimal superRate = toSuperRate(record.get(3));
            String paymentPeriod = record.get(4);
            return new EmployeePayment(firstName, lastName, annualSalary, superRate, paymentPeriod);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new CsvReaderException("Input format invalid for line " + record);
        }
    }

    private int toAnnualSalary(String salaryString) {
        try {
            return Integer.parseInt(salaryString);
        } catch (NumberFormatException e) {
            throw new CsvReaderException(format("Salary is not a valid number, was %s", salaryString), e);
        }
    }

    private BigDecimal toSuperRate(String superRateString) {
        if (!superRateString.matches(".*%")) {
            throw new CsvReaderException(format("Super rate is not a valid percentage, was %s", superRateString));
        }
        String superRateNumberPart = superRateString.substring(0, superRateString.length() - 1);
        try {
            BigDecimal superRatePercentage = new BigDecimal(superRateNumberPart);
            return superRatePercentage.divide(_100, superRatePercentage.scale() + 2, RoundingMode.UNNECESSARY);
        } catch (NumberFormatException e) {
            throw new CsvReaderException(format("Super rate is not a valid percentage, was %s", superRateString), e);
        }
    }
}
