package com.myob.test.payslip.application;

import java.math.BigDecimal;

public class EmployeePayment {

    private final String firstName;
    private final String lastName;
    private final int annualSalary;
    private final BigDecimal superRate;
    private final String paymentPeriod;

    /**
     *
     * @param firstName
     * @param lastName
     * @param annualSalary
     * @param superRate should be a fraction, e.g., a rate of 9% should come as 0.09
     * @param paymentPeriod
     */
    public EmployeePayment(String firstName, String lastName, int annualSalary, BigDecimal superRate,
                           String paymentPeriod) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.annualSalary = annualSalary;
        this.superRate = superRate;
        this.paymentPeriod = paymentPeriod;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAnnualSalary() {
        return annualSalary;
    }

    public BigDecimal getSuperRate() {
        return superRate;
    }

    public String getPaymentPeriod() {
        return paymentPeriod;
    }
}
