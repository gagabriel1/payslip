package com.myob.test.payslip.application;

import com.myob.test.payslip.domain.Employee;
import com.myob.test.payslip.domain.IncomeTaxRates;
import com.myob.test.payslip.domain.Payslip;
import com.myob.test.payslip.domain.PayslipRepository;

import java.util.Iterator;

/**
 * This generates the payslips for a given collection of employees and periods.
 * Uses the given IncomeTaxRates table to calculate taxes.
 * The payslips will be saved to the given Payslip repository.
 */
public class PayslipGenerationApplication {

    private final IncomeTaxRates incomeTaxRates;
    private final PayslipRepository payslipRepository;

    public PayslipGenerationApplication(IncomeTaxRates incomeTaxRates, PayslipRepository payslipRepository) {
        this.incomeTaxRates = incomeTaxRates;
        this.payslipRepository = payslipRepository;
    }

    /**
     * Generates and saves the payslips for the given employees and periods.
     * <p>
     * If there is any problem generating or saving a given payslip, the processing will stop.
     *
     * @param command
     * @throws IllegalArgumentException                                if any of the input values is invalid
     * @throws com.myob.test.payslip.domain.PayslipRepositoryException if there is problem saving the payslip
     */
    public void generatePayslips(GeneratePayslipsCommand command) {
        Iterator<EmployeePayment> employeePayments = command.getEmployeePayments();
        while (employeePayments.hasNext()) {
            generatePayslip(employeePayments.next());
        }
    }

    private void generatePayslip(EmployeePayment employeePayment) {
        Employee employee = new Employee(employeePayment.getFirstName(),
                                         employeePayment.getLastName(),
                                         employeePayment.getAnnualSalary(),
                                         employeePayment.getSuperRate());
        Payslip payslip = new Payslip(employee, employeePayment.getPaymentPeriod(), incomeTaxRates);
        this.payslipRepository.save(payslip);
    }
}
