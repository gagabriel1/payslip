package com.myob.test.payslip.application;

import java.util.Iterator;

/**
 * This represents a request to generate the payslips for the given list of employees and payment period.
 */
public class GeneratePayslipsCommand {

    /*
     * This should contain all the specific information to be able to generate the payslip
     * for the given list of employees and payment period. That is, general information like
     * the income tax rates should not be included here.
     */

    private Iterator<EmployeePayment> employeePayments;

    public GeneratePayslipsCommand(Iterator<EmployeePayment> employeePayments) {
        this.employeePayments = employeePayments;
    }

    public Iterator<EmployeePayment> getEmployeePayments() {
        return employeePayments;
    }
}
