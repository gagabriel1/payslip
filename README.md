Payslip Generation Tool
============

System Requirement
------------
* Git (only to get the code)
* Maven (only to build the application, has only been tested with version 3.x)
* Java 8
* It has been tested to run in MacOSX only, but Linux should also be fine 
and most likely Windows too

How to Run
------------

After getting the code, run 

```
$ mvn clean verify
```

to run all tests.

The above should also generate a "fat" JAR including all dependencies.

To run the JAR:

```
$ java -jar target/payslip-1.0-SNAPSHOT.jar {input file}
```

where {input file} should be a valid CSV file in a writable folder.

The output will be generated in same folder as the input, with the same name 
but adding "-out.csv" to the input file name without the extension. 

A valid input content would be 

```
David,Rudd,60050,9%,01 March – 31 March
Ryan,Chen,120000,10%,01 March – 31 March
```

Code Considerations
------------

* The code was developed following the hexagonal architecture ideas
* Comments have only been added where they add some relevant info to the method, 
effort was put into making the method name self explanatory
* There is no logging, errors are printed to standard output.
For a production application, logging would be added to enable troubleshooting.
* Some of the specification could be expanded to allow handling salaries with cents;
right now, only integer salaries are supported. It is thus easier and more consistent
to use int to represent money amounts. It would be desirable to use BigDecimal or 
a library like Joda Money to handle money amounts to avoid or make explicit rounding issues.

